Extensión al plugin de BigBlueButton de Moodle
============================================================

Esta es una guía técnica sobre la extensión al plugin de BigBlueButton de Moodle.
Con esta extensión usted podrá crear servidores virtuales en demanda para gestionar
las videoconferencias de BigBlueButton creadas desde Moodle. La administración de los servidores
virtuales se hace através del gestor de nube OpenStack.


**Terminología básica**

Es importante conocer los siguientes términos antes de leer la documentación:

* **Servidor de BBB:** software BigBlueButton (desarrollado por BlindSideNetworks) para gestionar videoconferencias.
* **Plugin de BBB:** componenete (plugin) de Moodle que permite crear actividades de videoconferencias usando el software BigBlueButton desde los cursos de Moodle.
* **Servidor de OpenStack:** servidor con una instalación del gestor de Nube OpenStack.
* **Imagen de un servidor de BBB:** imagen de disco funcional con el software de BigBlueButton instalado y listo para usarse.
* **Cron job de integración con OpenStack**: Tarea agendada de Moodle que se encarga de gestionar los servidores de BBB con el servidor de OpenStack.


Estructura del plugin y guías técnicas
--------------------------------------

.. toctree::
   :maxdepth: 2

   estructura_plugin
   guia_programador

Extensión a las funcionalidades
-------------------------------

.. toctree::
   :maxdepth: 2

   servidores_bbb_en_demanda
   bitacoras_openstack
   reservacion_videoconferencias
   notificaciones
   resiliencia
.. Indices and tables
.. ==================
..
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
