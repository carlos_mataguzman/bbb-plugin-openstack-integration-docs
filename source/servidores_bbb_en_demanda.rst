.. _servidores_bbb_en_demanda:

Servidores BBB en demanda
=========================
La parte del plugin extentido relacionada con los servidores en demanda es el componente principal que
permite la comunicación con el servidor de OpenStack con la intención de crear los servidores de BBB que
albergan cada una de las videoconferencias. Para realizar el manejo de estos servidores se utiliza el
cron job de integración con OpenStack, el cual es el encargado de realizar las solicitudes al servidor de
OpenStack a través de una biblioteca de terceros llamada `PHP OpenCloud`_. Del mismo modo es necesario tener
configurado un repositorio o un servidor web donde se puedan accesar los insumos de creación de los stacks
que alberguen a los servidores BBB.

La extensión se puede desactivar en todo momento y de esta forma seguir usando el plugin original baso
su modelo de servidor único. Además esta extensión permite aplicar políticas de uso orientadas a generar un
mayor orden en la forma de organizar las videoconferencias de BBB en Moodle. Al ser los servidores en demanda
la parte fundamental de la extensión, es indispensable que esté activada si se desean usar cualquiera de los otros
módulos descritos en esta documentación.

Con esta extensión es posible:

* Activar o desactivar la opción de servidores en demanda.
* Usar los otros módulos desarrollados bajo el proyecto de extensión del plugin de BBB.
* Definir las configuraciones del proyecto de OpenStack encargado de gestionar las máquinas virtuales.
* Definir los insumos de creación de los *stacks* que albergaran los servidores de BBB en OpenStack (como parámetros o plantilla de creación del servidor).
* Definir de forma predeterminada las duraciones de videoconferencias.
* Definir un tiempo de gracia que se agrega al final de todas las videoconferencias antes de destruirlas.
* Implementar políticas de uso como el tiempo mínimo y el tiempo máximo(con cuánta antelación) para crear una videoconferencia.
* Activar o desactivar las configuraciones relacionadas a los servidores BBB en la interfaz de administración del plugin.


Cambios a nivel de base de datos
--------------------------------
A continuación se describen los cambios a nivel de base de datos:

**Tabla mdl_bigbluebutton**

Se agregaron los siguientes campos:

.. tabularcolumns:: |L|C|C|

+---------------------+-------------------------------+---------------+
| Campo               | Contenido                     | Tipo de dato  |
+=====================+===============================+===============+
| bbb_meeting_duration| Duación de la videoconferencia| bigint        |
+---------------------+-------------------------------+---------------+

Este campo se usa para realizar acciones como agendar la destrucción de
las máquinas y saber la disponibilidad de recursos a la hora de
reservar.

**Creación de tabla mdl_bigbluebutton_openstack**

Se creó una tabla para el manejo de todas aquellas videoconferencias creadas bajo la modalidad de
servidores en demanda. La tabla contiene los siguientes campos:

.. tabularcolumns:: |L|C|C|

+-------------------------+----------------------------------------------------+------------+
| Campo                   | Contenido                                          |Tipo de dato|
+=========================+====================================================+============+
| id                      | Identificador dentro de la tabla                   | bigint     |
+-------------------------+----------------------------------------------------+------------+
| stack_name              | Nombre del *stack* dentro de OpenStack             | char(256)  |
+-------------------------+----------------------------------------------------+------------+
| meetingid               | ID de la conferencia de BBB asociada               | char(256)  |
+-------------------------+----------------------------------------------------+------------+
| courseid                | ID del curso asociado a la videoconferencia        | char(256)  |
+-------------------------+----------------------------------------------------+------------+
| bbb_server_status       | Estado del servidor de BBB durante su ciclo de vida| char(256)  |
+-------------------------+----------------------------------------------------+------------+
| bbb_server_url          | URL para ingresar a la videoconferencia            | char(256)  |
+-------------------------+----------------------------------------------------+------------+
| bbb_server_shared_secret| *shared secret* asociado a la videoconferencia     | char(256)  |
+-------------------------+----------------------------------------------------+------------+
| meeting_duration        | Duración de la videoconferencia en minutos         | bigint     |
+-------------------------+----------------------------------------------------+------------+
| openingtime             | Tiempo en el que comienza la videoconferencia      | bigint     |
+-------------------------+----------------------------------------------------+------------+
| deletiontime            | Tiempo agendado para destruir la videoconferencia  | bigint     |
+-------------------------+----------------------------------------------------+------------+
| conference_name         | Nombre de la videoconferencia asociada             | char(256)  |
+-------------------------+----------------------------------------------------+------------+
| user_name               | Nombre del usuario que creó la videoconferencia    | char(256)  |
+-------------------------+----------------------------------------------------+------------+
| course_name             | Nombre del curso asociado a la  videoconferencia   | char(256)  |
+-------------------------+----------------------------------------------------+------------+

Cambios a nivel de código
--------------------------

Los cambios a nivel de código necesarios para el funcionamiento de este módulo se relacionan con la lógica del módulo, con base de datos,
interfaz del módulo y con nuevas configuraciones del plugin. Es importante destacar que todos los cambios aparecen únicamente si se activa
la función de servidores en demanda en las configuraciones del plugin de BBB.

**Lógica de la extensión del plugin de BBB**

Debido a que la extensión del plugin busca ser lo menos invasiva posible, su estrategia consiste en interceptar las solicitudes de creación, edición o eliminación
de las videoconferencias con el fin de almacenar la información relacionada en la tabla ``mdl_bigbluebutton_openstack`` creada para la manejar la información de los
servidores generados bajo una modalidad de demanda. El objetivo final es tener al tiempo de apertura de la videoconferencia un valor con el URL del servidor de BBB y su
``shared secret`` (hash de seguridad) que pueda ser consultado al momento de construir la interfaz gráfica de ingreso a la videoconferencia en Moodle. Si no es posible
tener estos valores la videoconferencia se declarará como fallida y se mostrarán mensajes de error a los usuarios afectados.

El funcionamiento del módulo se basa en la creación de una tarea agendad dentro de Moodle encargada de comunicarse periodicamente con el servidor
de OpenStack con el fin de realizar tres subtareas: **actualización de estado de servidores de BBB**, **creación de nuevos servidores de BBB** cuyas conferencias
estén prontas a iniciar y la **eliminación de los servidores de BBB** cuyas videoconferencias ya hayan finalizado. Cada uno de estos cambios se registra dentro
de la tabla de la base de datos relacionada a openstack con el propósito de tener actualizado el estado del servidor de BBB en todo momento ya sea que esté esperando
a ser creado, este en proceso de creación, listo para usarse, eliminado o el algún estado de error.

Para todo el funcionamiento relacionado con el cron job de integración con OpenStack se agregó una carpeta completa ``clases/openstack`` la cual se explicará con detalle a continuación:

- ``tasks/openstack_async_communication.php``: es la primer clase que ejcuta el cron job. Contiene el código inicial debe ejecutar el cron job de integración de OpenStack y que instancia y llama a la clase de tareas.

- ``moodle_bbb_openstack_stacks_management_tasks.php``: esta es la clase principal de la lógica. Es la encargada de gestionar las comunicaciones, solicitar el envío de mensajes y la generación de las bitácoras relacionadas con la ejecución del cron job de integración de OpenStack. Realiza 3 subtareas principales: creación de servidores, eliminación de servidores y actualización de servidores.

- ``meeting_setup,php``: esta clase se encarga de ejecutar las subtareas asignadas por  ``moodle_bbb_openstack_stacks_management_tasks,php`` (creación, actualización y eliminación) en los objetos de videoconferencia.

- ``bbb_host_management,php``: esta clase se encarga de recibir las solicitudes de la clase ``meeting_setup.php`` para gestionar los servidores de BBB y hacer las operaciones directamente relacionadas con los stacks.

- ``bbbb_stack``: esta clase implementa los métodos relacionados al objeto ```Stack`` según la biblioteca de terceros utilizada. Es el nivel más bajo de abstracción al que se llegó en el código.

- ``helpers.php``: clase con funciones variadas como consultas a la base de datos o cálculos relacionados al estado de los servidores BBB. Es usada únicamente por la clase ``moodle_bbb_openstack_stacks_management_tasks.php``.

En cuanto a las políticas de uso estas se integraron directamente como validaciones al formulario de creación o edición de videoconferencias de Moodle. Del mismo modo se modificó
la interfaz con el fin de que fuera más amigable y clara para el usuario. A continuación se resumen los cambios:

- En ``mod_form.php`` se agrega como obliigatorio el campo duración de videoconferencia para que los usuarios escojan entre las opciones disponibles. Del mismo modo se modificó la hora de cierre para que fuera expresada en minutos y se muestra la hora de finalización de la videoconferencia en todo momento. Se hacen  validaciones relacionadas con los tiempos de apertura, cierre y duración de la videoconferencia.

- En ``lang`` se agregan *strings* relacionados con los mensajes de validación y los elementos agregados a la interfaz.

- En ``mod_form.js`` se agrega la función que recalcula el tiempo de finalización de forma automática si alguno de los valores de duración o apertura se modifica.


Para la vista de ingreso a las videoconferencia se efectuaron los siguientes cambios:

- En ``view.php`` se creó la lógica para construir la interfaz de ingreso a la videoconferencia con los valores del servidor en demanda o mostrar un mensaje de error en caso de que el proceso falle. También se muestran detalles del proceso de creación a los administradores o creadores de la videoconferencia. Del mismo modo se agrega la hora del servidor.

- En ``lang`` se agregan *strings* relacionados con los mensajes de error que se muestran al usuario y los mensajes de resumen de estado.

- En ``module.js`` se agrega la funcionalidad que desplega la hora del servidor.


Otras funciones creadas o modificadas por la extensión del plugin  se describen a continuación:

- En ``index.php`` se modifica la lógica para que haga uso de los datos sobre servidores en demanada en caso que esté activada esta opción. Esta función se podría mejorar si con anteriordad se consulta si la videoconferencia ya se encuentra dentro de la tabla ``mdl_bigbluebutton_openstack``.

- En ``lib.php`` se crearon las siguientes funciones:

  - ``bigbluebuttonbn_meeting_is_duplicated``: verifica si una videoconferencia está duplicada.

  - ``bigbluebuttonbn_change_duplication``: hace única una videoconferencia anteriormente duplicada.

  - ``bigbluebuttonbn_get_min_openingtime``: devuelve el momento más próximo permitido para que inicie una videoconferencia.

  - ``bigbluebuttonbn_get_max_openingtime``: devuelve el momento más anticipado permitido para que inicie una videoconferencia.

  - ``bigbluebuttonbn_create_or_update_os_conference``: función que agrega o edita una videoconferencia de la tabla ``mdl_bigbluebutton_openstack``.

  - ``bigbluebuttonbn_delete_os_conference``: función que elimina una videoconferencia de la tabla ``mdl_bigbluebutton_openstack``.

  - ``bigbluebuttonbn_get_os_stack_name``: función que devuelve el nombre del *stack* relacionado a una videoconferencia.

  - ``bigbluebuttonbn_get_openstack_meetingid_by_id``: función que devuelve el id en la tabla ``mdl_bigbluebutton_openstack`` de una videoconferencia.

  - ``bigbluebuttonbn_openstack_managed_conference``: función que devuelve si una videoconferencia fue creada bajo la modalidad de servidores en demanda.


- En ``localib.php`` se crearon las siguientes funciones:

  - ``bigbluebuttonbn_get_meeting_server_url``: esta función se encarga de recuperar de la ``tabla mdl_bigbluebutton_openstack`` el URL del servidor de BBB correspondiende.

  - ``bigbluebuttonbn_get_meeting_shared_secret``: esta función se encarga de recuperar de la ``tabla mdl_bigbluebutton_openstack`` el URL del servidor de BBB correspondiende.

  - ``bigbluebuttonbn_get_previous_setting``: esta función devuelve el valor anterior de algún campo de la base de datos especificado.

  - ``get_meeting_deletion_time_minutes``: esta función calcula el momento en el tiempo en que se deberá eliminar la videoconferencia.

- En ``localib.php`` se modificaron las siguientes funciones:

  - ``bigbluebuttonbn_bbb_broker_get_meeting_info``: se encarga de obtener información del servidor de BBB que alberga la videoconferencia.

  - ``bigbluebuttonbn_bbb_broker_do_end_meeting``: se encarga de terminar la videoconferencia en el servidor de BBB que la alberga.

**Cambios en base de datos**

- Se modificaron los archivos ``db/instal.xml``, ``db/upgrade.php`` y ``version.php`` para realizar los cambios a nivel de base de datos.

- Se creó una tarea agendada ``db/tasks`` la cual contiene la especificación de la periodicidad de ejecución de la tarea agendada, su nombre y el archivo donde está el código que se debe ejecutar.

- En ``lang``: se agregan *strings* relacionados con la creación de la tarea agendada.

**Nuevas configuraciones del plugin**

Se agregaron las siguientes configuraciones administrativas:



- ``bigbluebuttonbn_openstack_integration``: indica si se desea activar la integración con OpenStack. Si no está marcada el plugin tendrá el mismo funcionamiento que el plugin original de BBB.

- ``bigbluebuttonbn_openstack_username``: nombre de usuario válido para gestionar máquinas virtuales.

- ``bigbluebuttonbn_openstack_password``: contraseña asociada al nombre de usuario anterior.

- ``bigbluebuttonbn_openstack_tenant_id``: proyecto asociado al usuario anterior.

- ``bigbluebuttonbn_heat_url``: URL relacionado con el API de Heat de OpenStack.

- ``bigbluebuttonbn_heat_region``: región de Heat asociada al proyecto de OpenStack.

- ``bigbluebuttonbn_json_stack_parameters_url``: URL del archivo que contiene los parámetros que completan la plantilla de Heat del punto anterior. Debe estar en formato JSON y estar accesible a través del protocolo HTTP o HTTPS.

- ``bigbluebuttonbn_yaml_stack_template_url``: URL del archivo que contiene la plantilla de Heat necesaria para crear los servidores virtuales de BBB. Debe estar en formato YAML y estar accesible a través del protocolo HTTP o HTTPS.

- ``bigbluebuttonbn_meeting_durations``:  lista con las posibles opciones de duración de videoconferencias que se le muestran a los usuarios al momento de crear una videoconferencia.

- ``bigbluebuttonbn_conference_extra_time``: tiempo extra o tiempo de gracia en minutos que se agrega a todas las videoconferencias creadas.

- ``bigbluebuttonbn_min_openingtime``:tiempo mínimo para programar una videoconferencia, es decir qué tan pronto se puede programar.

- ``bigbluebuttonbn_max_openingtime``: tiempo máximo para programar una videoconferencia, es decir, qué tan anticipado se puede programar.




- ``bigbluebuttonbn_conference_extra_time``: Tiempo extra para los participantes de la videoconferencia antes de destruir el servidor BBB donde se ejecuta.


.. _PHP OpenCloud: http://docs.php-opencloud.com/en/latest/
