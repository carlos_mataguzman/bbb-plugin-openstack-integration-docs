.. _estructura_plugin:

Estructura del plugin extendido de BBB
======================================

El plugin de BigBlueButton es un plugin «de actividad» según la
clasificación propia de Moodle. Es importante destacar que todos los
plugins mantienen una estructura muy similar, más detalles de esto se
pueden encontrar en la `Documentación Oficial de Moodle`_. A
continuación se describe la estrucutra funcional a nivel de componentes, código del
plugin y base de datos. En la última sección se listan los archivos agregados o modificados
por la extensión del plugin de BBB.

Estructura a nivel de componentes
#################################

Las funciones de los componentes que integran la solución son los siguientes:

* **Servidor de Moodle:** es donde reside el código del plugin extendido. Desde la interfaz web se administran las configuraciones generales del plugin y donde los usuarios solicitan y acceden a las videoconferencias.
* **Plugin de BBB:** es el encargado de gestionar toda la lógica relacionada con la administración de las videoconferencias de BigBlueButton creadas desde la interfaz web de Moodle.
* **Cron job de integración con OpenStack:** es una tarea agendada que corre continuamente en el servidor de Moodle y que se encarga de comunicarse con el servidor de OpenStack.
* **Servidor de OpenStack:** es el encargado de administrar los servidores disponibles para el alojamiento de las videoconferencias de BigBlueButton.

Estructura a nivel de base de datos
###################################
A nivel de base de datos se usan y administran las siguientes tablas:

-  **Tabla** ``mdl_bigbluebutton`` **:** tabla que maneja el funcionamiento
   original del plugin y los datos relacionados con las
   videoconferencias.

-  **Tabla** ``mdl_bigbluebuttonbn_logs`` **:** tabla que contiene las
   bitácoras relacionadas al funcionamiento original del plugin y las
   acciones que realizan los usuarios dentro del servidor de Moodle.

-  **Tabla** ``mdl_bigbluebuttonbn_openstack`` **:** tabla que contiene los
   datos relacionados con las videoconferencias manejadas através de
   OpenStack.

-  **Tabla** ``mdl_bigbluebutton_os_logs`` **:** tabla que registra los
   eventos relacionados con elciclo de vida de los servidores de BBB,
   cuando la integración con OpenStack del plugin está habilitada.

-  **Tabla** ``mdl_bigbluebuttonbn_reservations`` **:** tabla que almacena las
   reservaciones de videoconferencias creadas por parte de los usuarios. Permite saber si
   hay recursos disponibles al momento de reservar una conferencia.

Estructura a nivel de código
############################
La organización de los archivos dentro del plugin es la siguiente, se describen todas las carpetas
y los archivos principales:

* ``backup/``: Carpeta con archivos necesarios para realiza un respaldo de los datos del plugin en caso que se realice un respaldo de la instancia de Moodle.
* ``classes/``: Carpeta con clases para funciones específicas del plugin.
    * ``events/``: Carpeta con clases que describen los eventos del plugin a integrar en el API de eventos de Moodle.
    * ``openstack/``: Carpeta con clases encargadas de la comunicación con OpenStack.
    * ``tasks/``: Carpeta con clases para el manejo de las *scheduled tasks* (tareas agendadas) de Moodle.
* ``db/``: Carpeta con los archivos relacionados con la base de datos.
    * ``access.php``: Archivo que describe las capacidades del plugin.
    * ``install.php``: Archivo que permite ejecutar código PHP después de haber agregado las tablas del plugin a la base de datos.
    * ``install.xml``: Archivo que descirbe las tablas que usa el plugin.
    * ``log.php``: Archivo que describe los mensajes generados por el plugin al usar el sistema de bitácoras de Moodle.
    * ``messages.php``: Archivo que describe los tipos de mensajes enviados por el plugin.
    * ``tasks.php``: Archivo con las configuraciones de las *scheduled tasks* creadas por el plugin.
    * ``uninstall.php``: Archivo que permite ejecutar código PHP después de eliminar las tablas del plugin durante una desinstalación.
    * ``upgrade.php``: Archivo que describe, en código PHP, los pasos necesarios para generar las tablas en la base de datos. Aplica al instalar o actualizar el plugin.
* ``lang/``: Carpeta con los archivos que contienen los *strings* del plugin. Se dividen en carpetas por idioma.
* ``log/``: Carpeta con bitácoras de errores relacionadas con la integración con OpenStack.
* ``openstack_interface/``: Carpeta con los archivos que manejan las interfaces web extra para la integración con OpenStack.
* ``pix/``: Archivo con logos e imágenes usados por el plugin.
* ``vendor/``: Carpeta con el código de bibliotecas de terceros que se usan en el plugin.
* ``bbb_broker.php``: Archivo que funciona como mediador para las acciones del servidor de BBB y el plugin de Moodle.
* ``bbb_view.php``: Archivo que maneja la creación de la ventana donde se realiza la videoconferencia.
* ``composer.json``: Archivo usado por *Composer* para autogestionar las bibliotecas de terceros.
* ``config-dist.php``: Archivo donde se definen los valores por defecto para varias de las configuraciones generales del pluign.
* ``import_view.php``: Archivo con la interfaz de importación de grabación de videoconferencias.
* ``index.php``: Archivo que maneja la interfaz que lista las instancias de BigBlueButton de un curso.
* ``JWT.php``: Clase con una implementación de *JSON Web Token* (JWT) para codificar las comunicaciones entre el plugin y el servidor de BBB.
* ``lib.php``: Funciones para la interacción del plugin y el core de Moodle.
* ``localib.php``: Funciones entre componentes del plugin.
* ``mod_form.js``: Funciones de JavaScipt asociadas al formulario de creación de las videocoferencias de la interfaz de Moodle.
* ``mod_form.php``: Archivo con el formulario de creación de las videocoferencias de la interfaz de Moodle.
* ``module.js``: Archivo con funciones en JavaScript para manejarlas mediante módulos de YUI. Moodle va a dejar de usar YUI y recomienda usar JQuery con AMD.
* ``README.md``: Archivo que documenta información general del proyecto.
* ``RELEASENOTES``: Archivo que recopila los cambios en las versiones del plugin.
* ``settings.php``: Archivo que maneja la interfaz de las configuraciones generales del plugin.
* ``thirdpartylibs.xml``: Archivo que describe las bibliotecas de terceros que usa el plugin.
* ``version.php``: Archivo que maneja la versión del plugin visible para el servidor de Moodle.
* ``view.php``: Archivo que describe la interfaz mostrada al ingreinstancias de videoconferencia.

Archivos modificados/agregados
##############################
Es importante mencionar que todas las modificaciones se encuentran fueron marcadas usando las líneas
``/*--- OpenStack integration ----/*`` y ``/*---- end of OpenStack integration ----*/`` esto con el fin de poder identificar mejor los cambios en el código.
A continuación se muestra la lista de archivos modificados o creados durante el desarrollo del plugin extendido de BBB:

* Todos los archivos de la carpeta ``classes/openstack``
* Todos los archivos de la carpeta ``openstack/interface``
* ``lang/es_mx/bigbluebuttonbn.php``
* ``lang/en/bigbluebuttonbn.php``
* ``index.php``
* ``locallib.php``
* ``mod_form.js``
* ``view.php``
* ``db/upgrade.php``
* ``db/messages.php``
* ``db/install.xml``
* ``settings.php``
* ``lib.php``
* ``mod_form.php``
* Los archivos de la carpeta ``vendor``

.. _Documentación Oficial de Moodle: https://docs.moodle.org/dev/Activity_modules
