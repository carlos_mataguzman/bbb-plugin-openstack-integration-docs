.. _resiliencia:

Módulo de resiliencia
=====================
Este es un módulo administrativo que permite ejecutar más de una vez las subtareas del cron job de
integración con OpenStack que inicialmente generan un error, con el fin de intentar resolverlos sin necesidad
de intervención humana o permitiendo la intervención humana antes de declarar como fallida una operación.
El módulo representa un primer paso en la generación de un servicio resiliente lo cual hace al modelo de
ciclo de vida de las máquinas de BBB en uno más completo y robusto. Este módulo requiere que esté activado
la opción de servidores BBB en demanda.

Con este módulo es posible:

* Activar o desactivar la resiliencia.
* Definir la cantidad de intentos que se desean efectuar por intento de creación de servidor de BBB.
* Definir la cantidad de intentos que se desean efectuar por intento de creación de servidor de BBB.

Cambios a nivel de base de datos
--------------------------------

**Tabla mdl_bigbluebutton**

Se agregaron los siguientes campos:

+-------------------------+----------------------------------------------------------------------------+------------+
| Campo                   | Contenido                                                                  |Tipo de dato|
+=========================+============================================================================+============+
| creation_attempts       | Cantidad de solicitudes de creación realizadas al servidor de OpenStack    | bigint     |
+-------------------------+----------------------------------------------------------------------------+------------+
| deletion_attempts       | Cantidad de solicitudes de eliminación realizadas al servidor de OpenStack | char(256)  |
+-------------------------+----------------------------------------------------------------------------+------------+


Cambios a nivel de código
--------------------------
Los cambios a nivel de código necesarios para el funcionamiento de este módulo se relacionan con la lógica del módulo, con base de datos y
con nuevas configuraciones del plugin.

**Lógica del módulo**

El funcionamiento del módulo se basa en modificar la lógica de ejecución del cron job de integración con OpenStack con el fin de repetir hasta *n* veces las operaciones
de creación y eliminación donde *n* es un número definido por el administrador de Moodle como el máximo de repeticiones de cada una de estas subtareas del cron job. Estas
repeticiones se dan antes de declarar como fallida las videoconferencias. Si el módulo se desactiva entonces ante cada error se declarará la videoconferencia respectiva como fallida.

Se modificaron los siguientes archivos:

- ``moodle_bbb_openstack_stacks_management_tasks``: se agrega la lógica de reliencia en las funciones relacionadas con creación y eliminación de servidores de BBB con OpenStack.

**Cambios en base de datos**

Se modificaron los archivos ``db/instal.xml``, ``db/upgrade.php`` y ``version.php`` para realizar los cambios a nivel de base de datos.

**Nuevas configuraciones del plugin**

Se agregaron las siguientes configuraciones administrativas:

- ``bigbluebuttonbn_resiliency_module_enabled``: Habilitación o deshabilitación del módulo de resiliencia.
- ``bigbluebuttonbn_creation_retries_number``: Número de veces que se debe reintentar la solictud de creación de un servidor de BBB a OpenStack.
- ``bigbluebuttonbn_deletion_retries_number``: Número de veces que se debe reintentar la solictud de eliminación de un servidor de BBB a OpenStack.

Para esto se modificó tanto el archivo ``settings.php``, ``db/lang/`` y el archivo ``lib.php`` en la sección ``//----Admin settings``.
