.. _notificaciones:

Módulo de notificaciones
========================
Este es un módulo administrativo que permite al administrador de Moodle recibir mensajes
de correo informando sobre problemas relacionados con el cron job de integración de OpenStack.
Su objetivo es avisar de forma inmediata sobre los problemas que pueden estarse presentando al momento
de gestionar los servidores de BBB en demanda. El administrador puede decidir si activa o desactiva las notificaciones de mensajes.
Este módulo se hizo bajo la guía de `Message API`_ de Moodle. Este módulo requiere que esté activado
la opción de servidores BBB en demanda.

Con este módulo es posible:

* Activar o desactivar las notificaciones de los mensajes.
* Definir los correos a los cuales se deben enviar los mensajes de correo de acuerdo al tipo de mensaje.

Cambios a nivel de base de datos
--------------------------------

Este módulo no creó ninguna tabla dentro de la base de datos de moodle.
Lo único que se modificó de forma indirecta fue la tabla ``mdl_message_providers`` en la que se agregan los siguientes proveedores de mensajes:

* **openstack_conection_error**: para los mensajes relacionados con la conexión hacia el servidor de OpenStack.
* **openstack_task_error**: para los mensajes relacionados con las subtareas del cron job de integración de OpenStack.

Estos cambios permiten que se desactiven las notificaciones por defecto desde la interfaz de Moodle.

Cambios a nivel de código
--------------------------
Los cambios a nivel de código necesarios para el funcionamiento de este módulo se relacionan con la lógica del módulo, con base de datos y
con nuevas configuraciones del plugin.

**Lógica del módulo**

El funcionamiento del módulo se basa en enviar mensajes relacionados con errores de conexión o de las subtareas del cron job de integración de OpenStack.
Los mensajes de correo contienen detalles sobre las bitácoras generadas(propias del módulo de bitácoras) e informaición sobre las videoconferencias involucradas.
Si se usa en conjunto con el módulo de resiliencia los errores se notifican la primera vez que se presentan y la última vez que se intentan resolverlos.

El contenido del mensaje se genera durante la ejecución de las tareas del cron job de integración de OpenStack y posteriormente se hace uso de una clase de comunicación
de errores para dar el formato correcto a los datos que se van a enviar y hacer el uso de el API de mensajes de Moodle.

Se modificaron los siguientes archivos:

- ``moodle_bbb_openstack_stacks_management_tasks``: se agregan llamados a las funciones de envío de notificaciones cuando existen errores en el proceso de gestión de máquinas con OpenStack.


Se agregaron los siguientes archivos:


- ``classes/openstack/interfaces/error_communicator.php``:  interfaz de clase para ser implementada. Define las funciones básicas de construcción y envío de mensajes.

- ``clases/opensatack/openstack_error_communicator``:  Implementación de la interfaz ``error_communicator.php`` define las funciones de construción y envío de mensajes de acuerdo al tipo de mensaje que recibe.

- En ``lang``: se agregan *strings* relacionados con el contenido de los mensajes a ser enviados.


**Cambios en base de datos**

Se modificó el archivo ``db/messages.php`` donde se definen los provedores de mensajes según el API de mensajes de Moodle.

**Nuevas configuraciones del plugin**

Se agregaron las siguientes configuraciones administrativas:

- ``bigbluebutton_error_users_list_enabled``: Habilitación o deshabilita la lista de usuarios (distintos al administrador) para notificaciones de error.
- ``bigbluebuttonbn_openstack_connection_error_email_users_list``: Lista de correos para recibir notificaciones de errores de conexión con OpenStack.
- ``bigbluebuttonbn_openstack_task_error_email_users_list``: Lista de correos para recibir notificaciones de errores de tareas de OpenStack.

Para esto se modificó tanto el archivo ``settings.php``, ``db/lang/`` y el archivo ``lib.php`` en la sección ``//----Admin settings``.

.. _Message API: https://docs.moodle.org/dev/Messaging_2.0
