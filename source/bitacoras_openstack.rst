.. _bitacoras_openstack:

Módulo de bitácoras de gesitión de OpenStack
============================================
Este es un módulo administrativo que le permite al administrador de Moodle consultar las bitácoras
generadas al realizar la integración con OpenStack. El administrador puede descargar las bitácoras
o eliminarlas al filtrar por fechas. Este módulo no puede ser desactivado y es parte fundamental
de la extensión del plugin de OpenStack. Este módulo requiere que esté activado la opción de servidores BBB en demanda.

Con este módulo es posible:

* Generar bitácoras sobre todas las acciones que ejecuta el cron job de integración con OpenStack.
* Guardar en un archivo de disco los errores relacionados con la ejecución del cron job de integración con OpenStack.
* Habilitar o desactivar la opción de bitácora como archivo en disco.
* Descargar o eliminar información relacionada a las reservaciones de los usuarios de Moodle.

Cambios a nivel de base de datos
--------------------------------
A continuación se describen los cambios a nivel de base de datos:

**Creación de tabla mdl_bigbluebutton_os_logs**

Esta tabla es la encargada de registrar los eventos relacionados con la integración de OpenStack.

.. tabularcolumns:: |L|C|C|

+----------------+-------------------------------------------------------------------------------------------+------------+
| Campo          | Contenido                                                                                 |Tipo de dato|
+================+===========================================================================================+============+
| id             | Identificador dentro de la tabla                                                          | bigint     |
+----------------+-------------------------------------------------------------------------------------------+------------+
| event_time     | *Timestamp* del momento en que se registra el evento                                      | bigint     |
+----------------+-------------------------------------------------------------------------------------------+------------+
| stack_name     | Nombre del *stack* dentro de OpenStack                                                    | char(256)  |
+----------------+-------------------------------------------------------------------------------------------+------------+
| meetingid      | ID de la conferencia de BBB asociada                                                      | char(256)  |
+----------------+-------------------------------------------------------------------------------------------+------------+
| log_level*     | Nivel de severidad: emergency, alert, critical, error, warning, notice, information, debug| char(256)  |
+----------------+-------------------------------------------------------------------------------------------+------------+
| component      | Componenente relacionado: plugin de BBB, Openstack, conexión con OpenStack, Moodle        | char(256)  |
+----------------+-------------------------------------------------------------------------------------------+------------+
| conference_name| Nombre de la videoconferencia asociada                                                    | char(256)  |
+----------------+-------------------------------------------------------------------------------------------+------------+
| user_name      | Nombre del usuario que creó la videoconferencia                                           | char(256)  |
+----------------+-------------------------------------------------------------------------------------------+------------+
| course_name    | Nombre del curso asociado a la  videoconferencia                                          | char(256)  |
+----------------+-------------------------------------------------------------------------------------------+------------+
| event          | Nombre del evento que se registra                                                         | text       |
+----------------+-------------------------------------------------------------------------------------------+------------+
| event_details  | Descripción del evento, incluyendo mensajes de error.                                     | text       |
+----------------+-------------------------------------------------------------------------------------------+------------+

Cambios a nivel de código
--------------------------
Los cambios a nivel de código necesarios para el funcionamiento de este módulo se relacionan con la lógica del módulo, con base de datos,
interfaz del módulo y con nuevas configuraciones del plugin.

**Lógica del módulo**

El funcionamiento del módulo se basa en ingresar un resgistro en la tabla ``mdl_bigbluebutton_os_logs`` cada vez que una videoconferencia
creada bajo la modalidad de servidores en demanada es afectada (principalmente cada vez que se ejecuta el cron job  de integración con OpenStack)
y generar una entrada en el archivo de bitácora de disco cada vez que se genera un error en el proceso de gestión de la videoconferencia con OpenStack.
Los eventos de la bitácora se registran de acuerdo a su nivel de severidad:

* **ADD_BBB_CONFERENCE:** Creación de una conferencia por parte del usuario (**Information**)
* **UPDATE_BBB_CONFERENCE:** Edición de una conferencia por parte del usuairo (**Information**)
* **USER_DELETED_CONFERENCE:** Eliminación de una conferencia por parte del usuario antes de iniciar la creación del servidor de BBB (**Information**)
* **CANT_ACCESS_OPENSTACK:** Conexión con el servicio de Orquestación fallida (**Alert**)
* Solicitud de creación de servidor de BBB:

  * **CREATION_STARTED:** Solicitud de creación aceptada (**Information**)
  * **CREATION_REQUEST_FAILED:** Solicitud de creación rechazada (**Error**)

* Cambio en el estado de servidor de BBB:

  * **BBB_SERVER_READY:** Servidor de BBB listo (**Information**)
  * **CREATION_FAILED:** Fallló la creación de servidor de BBB (**Error**)

* Solicitud de eliminación de servidor de BBB:

  * **DELETION_STARTED:** Se acepta la solicitud de eliminación (**Information**)
  * **DELETION_START_FAILED:** Se rechaza la solicitud de eliminación (**Error**)

Los bitácoras pueden ser accesadas o eliminadas desde la interfaz del administrador de Moodle. Es importante aclarar que actualmente la bitácora en disco
se almacena en la ruta ``log/openstack_errors`` dentro del directorio del plugin, por lo cual se recomienda cambiar esto.

Se crearon bitácoras propias debido a que las proveídas por el API de Moodle no se ajustaban a los estándares deseados para la implementación de servicio
ya que carecen de un nivel aceptable de personalización.

Se modificaron los siguientes archivos:

- En ``lib.php`` se agrega una función existente:

  - ``bigbluebuttonbn_add_openstack_event``:  función encargada de agregar los eventos relacionados con las bitácoras de gesitión de OpenStack a la tabla ``mdl_bigbluebutton_os_logs``.


- En ``lib.php`` se modifican dos funciones existentes para crear los llamados a la función de inserción en las bitácoras de gesitión de OpenStack:

  - ``bigbluebuttonbn_delete_instance``:  función existente encargada de manejar el proceso de eliminación de videoconferencias.
  - ``bigbluebuttonbn_process_post_save``: función existente encargada de agregar y editar videoconferencias.

- En ``classes/openstack/moodle_bbb_openstack_stacks_management_tasks``: se hacen múltiples llamados para registrar en las bitácoras de gesitión de OpenStack los eventos del cron job de integración con OpenStack.

- En ``lang``: se agregan ''strings'' relacionados con la interfaz administrativa de OpenStack para descargar y eliminar los registros relacionados al módulo de bitácoras.

Se agregaron los siguientes archivos:

- ``classes/openstack/interfaces/exception_handler.php``:  interfaz de clase para ser implementada. Define las funciones básicas de manejo de excepciones.

- ``clases/opensatack/archive_log_exception_handler``:  Implementación de la interfaz ``exeception_handler.php``. Aquí se define donde se guardará el archivo de texto plano con bitácoras de errores.


**Creación de interfaz administrativa**

Se crean funciones en el directorio ``openstack_interface/`` que contiene todo el código que maneja las interfaces administrativas del módulo de reservaciones. También
se crearon funciones en ``locallib.php`` para el manejo de los registros, estas se encuentran en la sección ``//----Admin interface records management``.

**Cambios en base de datos**

Se modificaron los archivos ``db/instal.xml``, ``db/upgrade.php`` y ``version.php`` para realizar los cambios a nivel de base de datos.


**Nuevas configuraciones del plugin**

Se agregó las siguientes configuraciones administrativas:

- ``bigbluebuttonbn_error_log_file_enabled``: Habilitación o deshabilitación del archivo de bitácoras en disco.

Para esto se modificó tanto el archivo ``settings.php``, ``db/lang/`` y el archivo ``lib.php`` en la sección ``//----Admin settings``.
