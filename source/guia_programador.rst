.. _guia_programador:

Guía del programador
====================
Esta es una guía orientada al programador encargado de desarrollo del plugin.
Se cubren dos aspectos importantes el primero de ellos se relaciona con el manejo de los
repositorios y el código en sí. Posteriormente se describe una breve lista de tareas frecuentes.

Manejo de repositorios
######################
Los repositorios se deben manejar de forma ordenada se recomienda usar la metodología conocida como `Git flow`_,
de forma tal que se permita trabajar de forma separada y modular. Las entregas se deben marcar como
*releases* y la rama de desarrollo actualizada en todo momento. Los releases se deben marcar bajo la nomenclatura
**release** + **ucr-bbb** + **v{número de versión}**. La rama *master* o principal del repositorio se nombró
**universidad_de_costa_rica**, esta debe siempre contener la rama dedicada a producción.

Creación del código
###################
Debio a que se desea que realizar cambios poco invasivos sobre el plugin original es necesario
identificar las porciones de código que se agreguen o modifiquen mediante dos comentarios como los siguientes:

* ``/*---- OpenStack integration ----*/``` para el inicio de los cambios.
* ``/*---- end of OpenStack integration ----*/``` para el final de los cambios.

Del mismo modo se debe mantener el orden y la correcta documentación interna cuando sea necesario así como
seguir las políticas de desarrollo de plugins de Moodle y el uso de sus APIs siempre que sea posible.

Tareas frecuentes
#################
A continuación se muestra una guía de las posibles tareas frecuentes:

Realizar modificaciones a la base de datos de Moodle
----------------------------------------------------

**Nota:** *Es necesario comprender correctamente la estructura de la
base de datos antes de realizar cualquier cambio. Consulte `Estructura a nivel de Base de Datos`_ si tiene alguna duda.*

Para modificar la base de datos de moodle se deben afectar el archivo
``db/install.xml``. el archivo ``db/upgrade.php`` y ``version.php``.

Existen dos formas de realizar modificaciones a la base de dadtos de
Moodle:

1. Usar el editor XML desde la interfaz de Moodle: Forma más segura de
   hacer los cambios. Usar cuando no se tiene acceso directo a la base
   de datos. Seguir lo específicado en la `Documentación oficial de Mooodle`_.

2. Hacer los cambios directamente en el código del plugin: Recomendable
   cuando se tiene acceso directo a la base de datos. Forma más rápida
   de hacer los cambios.
3. Definir los cambios en el archivo ``db/install.xml``.
4. Definir los cambios en el archivo ``db/upgrade.php`` y cambiar el
   número de versión.
5. Cambiar el archivo ``version.php``.
6. Asegurar que los cambios se suban al servidor de Moodle.
7. Visitar ``Site Adminsitration > Notifications`` en la interaz de
   Moodle para migrar los cambios a la base de datos.

Revisar código de Moodle
------------------------

En muchos casos es mejor indagar en el código de Moodle con el fin de conocer
como se manejan algunas de las funcionalidades. Esto es determinante sobre todo
a la hora de intentar realizar tareas poco comunes y resulta necesario por la
falta de documentación que ofrece el software en varios de sus APIs y funciones.

Vaciar la caché del servidor de Moodle
--------------------------------------

Este comando es útil para refrescar el contenido de la página como por
ejemplo *strings* predefinidos. Debe estar ubicado en la raíz de la
instalación de Moodle.

``php  /admin/cli/purge_caches.php         //Usando php   ``
``docker-compose exec php-fpm php admin/cli/purge_caches.php   //Con docker-compose``

También se puede limpiar la caché visitando
``http://URLdelSitio/admin/cli/purge_caches/php``

Manejo de tareas agendadas (cron jobs de Moodle)
------------------------------------------------

Si desea saber más acerca de las tareas agendadas puede visitar la
`Documentación oficial de Moodle`_.

Es posible depurar las tareas usando la línea de comandos `Documentación oficial de Moodle`_

Administrar bibliotecas externas
--------------------------------

A continuación se muestra como manejar las dependencias relacionadas a
las bibiliotecas de terceros usando el programa *Composer*.

-  De estar trabajando en un contenedor con ``docker-compose`` abra una
   terminal ejecutando:
   ``docker-compose exec <nombre_del_contenedor> /bin/bash``
-  Es necesario de tener instalado el programa *Composer* para manejar
   bibliotecas de tereceros escritas en PHP.
-  Para instalarlo ejecute
   ``curl -sS https://getcomposer.org/installer | php``
-  Asegúrese de que exista el archivo
   ``mod/bigbluebuttonbn/composer.json``
-  Ingrese a ``mod/bigbluebuttonbn`` y ejecute
   ``php composer.phar install`` Esto creará una carpeta
   ``mod/bigbluebuttonbn/vendor`` con las bibliotecas que se declararon
   en ``mod/bigbluebuttonbn/composer.json`` y un archivo
   ``mod/bigbluebuttonbn/vendor/autoload.php``.

.. _Estructura a nivel de Base de Datos: https://docs.moodle.org/dev/Data_manipulation_API
.. _Documentación oficial de Moodle: https://docs.moodle.org/dev/Task_API
.. _Git flow: https://danielkummer.github.io/git-flow-cheatsheet/
