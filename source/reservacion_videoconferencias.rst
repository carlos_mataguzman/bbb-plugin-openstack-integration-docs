.. _reservacion_videoconferencias:

Módulo de reservación de videoconferencias
==========================================
Este es un módulo administrativo que permite al administrador de Moodle limitar
el número de videoconferencias simultáneas que se pueden ofrecer. Su objetivo es
administrar el servicio de videoconferencias de acuerdo a la cantidad de servidores
virtuales disponibles. Este módulo requiere que esté activado la opción de servidores BBB en demanda.

Con este módulo es posible:

* Definir el máximo de servidores virtuales disponibles desde la interfaz de configuraciones del plugin.
* Implementar una lógica de reservaciones que permita a los usuarios reservar únicamente si existen los recursos necesarios para gestionar la videoconferencia.
* Restringir los usuarios que pueden usar el plugin de BBB a través de una lista.
* Descargar o eliminar información relacionada a las reservaciones de los usuarios de Moodle.

Cambios a nivel de base de datos
--------------------------------
A continuación se describen los cambios a nivel de base de datos:

**Creación de tabla mdl_bigbluebutton_reservations**

Esta tabla es la encargada de registrar las reservaciones de videoconferencias hechas
por los usuarios de Moodle.

.. tabularcolumns:: |L|C|C|

+----------------+-------------------------------------------------------------------------------------------+------------+
| Campo          | Contenido                                                                                 |Tipo de dato|
+================+===========================================================================================+============+
| id             | Identificador dentro de la tabla                                                          | bigint     |
+----------------+-------------------------------------------------------------------------------------------+------------+
| start_time     | *Timestamp* del momento en que comienza la videoconferencia                               | bigint     |
+----------------+-------------------------------------------------------------------------------------------+------------+
| finish_time    | *Timestamp* del momento en que se liberan los recursos de la videoconferencia             | bigint     |
+----------------+-------------------------------------------------------------------------------------------+------------+
| begin date     | Fecha en que comienza la videoconferencia en un formato legible                           | char(256)  |
+----------------+-------------------------------------------------------------------------------------------+------------+
| end_date       | Fecha en que se liberan los recursos de la videoconferencia en un formato legible         | char(256)  |
+----------------+-------------------------------------------------------------------------------------------+------------+
| user           | Nombre y correo del usuario que creó la videoconferencia                                  | char(256)  |
+----------------+-------------------------------------------------------------------------------------------+------------+
| course         | Nombre y ID del curso asociado a la  videoconferencia                                     | char(256)  |
+----------------+-------------------------------------------------------------------------------------------+------------+
| meetingid      | ID de la conferencia de BBB asociada                                                      | char(256)  |
+----------------+-------------------------------------------------------------------------------------------+------------+

Cambios a nivel de código
--------------------------
Los cambios a nivel de código necesarios para el funcionamiento de este módulo se relacionan con la lógica del módulo, con base de datos,
interfaz del módulo y con nuevas configuraciones del plugin.

**Lógica del módulo**

El funcionamiento del módulo se basa en hacer una verificación a los parámetros contenidos en la solicitud
de creación de una conferencia. Estos parámetros se revisan para ver si existe suficientes recursos o no de
acuerdo a los datos de la tabla ``mdl_bigbluebutton_reservations``. Si se acepta la solicitud se ingresa un nuevo
campo en la base de datos.

Se modificaron los siguientes archivos:

- ``mod_form.php`` **:** se agregan validaciones al formulario de creación de una conferencia para verificar la disponibilidad de recursos. Para esto se modifica la función ``validation``. Se usó el `Lock API`_ de Moodle con el fin de controlar los accesos concurrentes a la base de datos.

- En ``localib.php`` se agregan las siguientes tres funciones:

  - ``bigbluebuttonbn_allow_user_reservation``: verifica que el usuario que está reservando esté autorizado para hacerlo.

  - ``bigbluebuttonbn_bbb_servers_availability``: verifica que haya disponibilidad de «recursos de videoconferencia» para la conferencia que se desea reservar.

  - ``bigbluebuttonbn_get_meeting_total_duration``: para revisar la disponibilidad de recursos y calcular el tiempo real de uso de recursos respectivamente.

- En ``lib.php`` se agregan tres funciones:

  - ``bigbluebuttonbn_create_or_update_bbb_servers_reservation``: agrega o actualiza reservaciones.

  - ``bigbluebuttonbn_add_meetingid_to_reservation``: relaciona una reservación con un servidor de BBB.

  - ``bigbluebuttonbn_delete_reservation``: eliminar una reservación existente.

- En ``lib.php`` se modifican dos funciones existentes:

  - ``bigbluebuttonbn_delete_instance``:  función existente encargada de manejar el proceso de eliminación de videoconferencias.
  - ``bigbluebuttonbn_process_post_save``: función existente encargada de agregar y editar videoconferencias.

- En ``lang``: se agregan *strings* relacionados con la interfaz administrativa de OpenStack para descargar y eliminar los registros relacionados al módulo de bitácoras.


**Cambios en base de datos**

Se modificaron los archivos ``db/instal.xml``, ``db/upgrade.php`` y ``version.php`` para realizar los cambios a nivel de base de datos.

**Creación de interfaz administrativa**

Se crean funciones en el directorio ``openstack_interface/`` que contiene todo el código que maneja las interfaces administrativas del módulo de reservaciones. También
se crearon funciones en ``locallib.php`` para el manejo de los registros, estas se encuentran en la sección ``//----Admin interface records management``.

**Nuevas configuraciones del plugin**

Se agregaron las siguientes configuraciones administrativas:

- ``bigbluebuttonbn_reservation_module_enabled``: Habilitación o deshabilitación del módulo de reservaciones.
- ``bigbluebuttonbn_max_simultaneous_instances``: Número máximo de instancias simúltaneas.
- ``bigbluebuttonbn_reservation_users_list_logic``: Logica de la lista de usuarios que pueden reservar. Puede ser de tipo ''whitelist'' o ''blacklist''.
- ``bigbluebuttonbn_authorized_reservation_users_list``: Lista con usuarios autorizados/bloqueados para crear una videoconferncia BBB.

Para esto se modificó tanto el archivo ``settings.php``, ``db/lang/`` y el archivo ``lib.php`` en la sección ``//----Admin settings``.

.. _Lock API: https://docs.moodle.org/dev/Lock_API
